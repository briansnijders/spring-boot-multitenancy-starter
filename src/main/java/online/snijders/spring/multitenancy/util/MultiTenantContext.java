package online.snijders.spring.multitenancy.util;

public class MultiTenantContext {

    private static ThreadLocal<String> currentTenantIdentifier = new ThreadLocal<>();

    public static String getCurrentTenantIdentifier() {
        return currentTenantIdentifier.get();
    }

    public static void setCurrentTenantIdentifier(String tenantIdentifier) {
        currentTenantIdentifier.set(tenantIdentifier);
    }

    public static void clear() {
        currentTenantIdentifier.set(null);
    }

}