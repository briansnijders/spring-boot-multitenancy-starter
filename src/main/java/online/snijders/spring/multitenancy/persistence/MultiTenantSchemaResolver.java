package online.snijders.spring.multitenancy.persistence;

import org.hibernate.context.spi.CurrentTenantIdentifierResolver;
import org.springframework.stereotype.Component;

import lombok.extern.slf4j.Slf4j;
import online.snijders.spring.multitenancy.util.MultiTenantContext;

@Component
@Slf4j
public class MultiTenantSchemaResolver implements CurrentTenantIdentifierResolver {

    private String defaultTenantIdentifier = "public";

    @Override
    public String resolveCurrentTenantIdentifier() {
        String currentTenantIdentifier = MultiTenantContext.getCurrentTenantIdentifier();

        return currentTenantIdentifier != null ? currentTenantIdentifier : defaultTenantIdentifier;
    }

    @Override
    public boolean validateExistingCurrentSessions() {
        return true;
    }
}
