package online.snijders.spring.multitenancy.persistence;

import java.sql.Connection;
import java.sql.SQLException;

import javax.sql.DataSource;

import org.springframework.stereotype.Component;

import lombok.extern.slf4j.Slf4j;

@Component
@Slf4j
public class MultiTenantConnectionProvider implements org.hibernate.engine.jdbc.connections.spi.MultiTenantConnectionProvider {

    private static final long serialVersionUID = 1348353870772468815L;
    private DataSource datasource;

    private String defaultTenantIdentifier = "public";

    public MultiTenantConnectionProvider(DataSource dataSource) {
        this.datasource = dataSource;
    }

    @Override
    public Connection getAnyConnection() throws SQLException {
        return datasource.getConnection();
    }

    @Override
    public void releaseAnyConnection(Connection connection) throws SQLException {

        connection.close();
    }

    @Override
    public Connection getConnection(String tenantIdentifier)
            throws SQLException {
        log.debug("Get connection for tenant {}", tenantIdentifier);
        final Connection connection = getAnyConnection();
        // Set the connection schema to the given tenant identifier (tenant data)
        connection.setSchema(tenantIdentifier);
        return connection;
    }

    @Override
    public void releaseConnection(String tenantIdentifier, Connection connection)
            throws SQLException {
        log.debug("Release connection for tenant {}", tenantIdentifier);
        // Set the connection schema to the default tenant identifier (system data)
        connection.setSchema(defaultTenantIdentifier);
        releaseAnyConnection(connection);
    }

    @Override
    public boolean supportsAggressiveRelease() {
        return false;
    }

    @SuppressWarnings("rawtypes")
    @Override
    public boolean isUnwrappableAs(Class unwrapType) {
        return false;

    }

    @Override
    public <T> T unwrap(Class<T> unwrapType) {
        return null;
    }

}